﻿using Microsoft.Win32;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Rozrost_Ziarna
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int kol, wr;
        private static Komorka[,] komorki;
        private Rectangle[,] plansza;
        private int[] sas;
        private Thread t;
        private int flag = 1;
        private int sasFlag;
        private int wbFlag;
        private int losFlag;
        private ComboBoxItem losowanie;
        private int liczbaZiaren;
        private Boolean brakPustych = true;
        private double[] ro;
        private double kro;
        private int wspk;
        private int tflag = 0;
        Random randomglobal = new Random();
        Random randomglobal2;
        private int liczbaPorekrystalizacji = 0;
        private int[] stany;
        private int liczMC;
        string fileName;
        int MCIterations, grainBoundariesEn;

        private WriteableBitmap bitmap;
        private Image image;


        public MainWindow()
        {
            InitializeComponent();
            this.t = new Thread(this.Run);
            this.sas = new int[8];
            for (int i = 0; i < 8; i++)
            {
                this.sas[i] = 0;
            }
        }

        private void Run()
        {
            switch (tflag)
            {
                case 1:
                    do
                    {
                        //  this.Dispatcher.Invoke(new Action(() => petla(0)));
                        Thread.Sleep(50);
                    } while (flag == 0);
                    break;
                case 2:
                    do
                    {
                        this.Dispatcher.Invoke(new Action(() => rekrystalizacja2(randomglobal)));
                        Thread.Sleep(50);
                    } while (liczbaPorekrystalizacji < wr * kol);
                    break;
                case 3:
                    do
                    {
                        //this.Dispatcher.Invoke(new Action(() => stworzNowyRandom(randomglobal2)));
                        this.Dispatcher.Invoke(new Action(() => MonteCarlo(randomglobal, randomglobal2)));
                        liczMC++;
                        // Thread.Sleep(500);
                    } while (true);
                    //break;
            }
            stop();
        }

        private void utworzPlansze(int k, int w)
        {
            this.plansza = new Rectangle[wr, kol];
            for (int i = 0; i < wr; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(grid.ActualHeight / wr);
                grid.RowDefinitions.Add(row);

                for (int j = 0; j < kol; j++)
                {
                    ColumnDefinition col = new ColumnDefinition();
                    col.Width = new GridLength(grid.ActualWidth / kol);
                    grid.ColumnDefinitions.Add(col);

                    Rectangle rec = new Rectangle();
                    grid.Children.Add(rec);
                    Grid.SetRow(rec, i);
                    Grid.SetColumn(rec, j);
                    rec.MouseLeftButtonUp += rectangleClick;
                    plansza[i, j] = rec;
                }
            }
        }

        private void utworzKomorki(int k, int w, Rectangle[,] pl)
        {
            komorki = new Komorka[w, k];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    Komorka newKomorka = new Komorka(255, pl[i, j]);
                    komorki[i, j] = newKomorka;
                }
            }
        }

        private Komorka[] sasiedztwoM(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k + 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w + 1, k - 1];

                    kom[2] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w + 1, k + 1];

                    kom[4] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];

                    kom[3] = komorki[w + 1, k - 1];

                    kom[4] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];

                    kom[3] = komorki[w + 1, k + 1];

                    kom[4] = komorki[w + 1, k - 1];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k - 1];

                    kom[1] = komorki[w - 1, k];

                    kom[2] = komorki[w - 1, k + 1];

                    kom[3] = komorki[w, k + 1];

                    kom[4] = komorki[w, k - 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[8];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w + 1, k + 1];

                    kom[4] = komorki[w + 1, k];

                    kom[5] = komorki[w + 1, k - 1];

                    kom[6] = komorki[w, k - 1];

                    kom[7] = komorki[w - 1, k - 1];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[8];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[w + 1, kol - 1];
                    kom[4] = komorki[wr - 1, kol - 1];
                    kom[5] = komorki[wr - 1, k];
                    kom[6] = komorki[w, kol - 1];
                    kom[7] = komorki[wr - 1, k + 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[0, k + 1];
                    kom[4] = komorki[0, 0];
                    kom[5] = komorki[0, kol - 1];
                    kom[6] = komorki[w, kol - 1];
                    kom[7] = komorki[w - 1, kol - 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k - 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w + 1, 0];
                    kom[4] = komorki[0, 0];
                    kom[5] = komorki[wr - 1, 0];
                    kom[6] = komorki[wr - 1, k];
                    kom[7] = komorki[wr - 1, k - 1];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k - 1];
                    kom[4] = komorki[0, k];
                    kom[5] = komorki[0, 0];
                    kom[6] = komorki[w, 0];
                    kom[7] = komorki[w - 1, 0];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[w + 1, k];
                    kom[5] = komorki[w + 1, kol - 1];
                    kom[6] = komorki[w, kol - 1];
                    kom[7] = komorki[w - 1, kol - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[w + 1, k - 1];
                    kom[4] = komorki[w + 1, k];
                    kom[5] = komorki[w + 1, 0];
                    kom[6] = komorki[w, 0];
                    kom[7] = komorki[w - 1, 0];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[w + 1, k - 1];
                    kom[5] = komorki[wr - 1, k - 1];
                    kom[6] = komorki[wr - 1, k];
                    kom[7] = komorki[wr - 1, k + 1];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k - 1];
                    kom[1] = komorki[w - 1, k];
                    kom[2] = komorki[w - 1, k + 1];
                    kom[3] = komorki[w, k + 1];
                    kom[4] = komorki[w, k - 1];
                    kom[5] = komorki[0, k - 1];
                    kom[6] = komorki[0, k];
                    kom[7] = komorki[0, k + 1];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[w + 1, k];
                    kom[5] = komorki[w + 1, k - 1];
                    kom[6] = komorki[w, k - 1];
                    kom[7] = komorki[w - 1, k - 1];
                }

            }

            return kom;
        }

        private Komorka[] sasiedztwoVN(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k - 1];

                    kom[2] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w, k - 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];

                    kom[3] = komorki[w, k - 1];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[4];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[wr - 1, k];
                    kom[3] = komorki[w, kol - 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[0, 0];
                    kom[3] = komorki[w, kol - 1];

                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[0, 0];
                    kom[3] = komorki[wr - 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k - 1];
                    kom[2] = komorki[0, k];
                    kom[3] = komorki[w, 0];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w, kol - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k - 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w, 0];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[wr - 1, k];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w, k - 1];
                }

            }

            return kom;
        }

        private Komorka[] sasiedztwoHL(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k + 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k + 1];

                    kom[3] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];

                    kom[3] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];

                    kom[3] = komorki[w + 1, k + 1];

                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k - 1];

                    kom[1] = komorki[w - 1, k];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w, k - 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[6];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k + 1];

                    kom[3] = komorki[w + 1, k];

                    kom[4] = komorki[w, k - 1];

                    kom[5] = komorki[w - 1, k - 1];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[6];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[wr - 1, kol - 1];
                    kom[4] = komorki[wr - 1, k];
                    kom[5] = komorki[w, kol - 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[0, k + 1];
                    kom[3] = komorki[0, 0];
                    kom[4] = komorki[w, kol - 1];
                    kom[5] = komorki[w - 1, kol - 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, 0];
                    kom[3] = komorki[0, 0];
                    kom[4] = komorki[wr - 1, k];
                    kom[5] = komorki[wr - 1, k - 1];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k];
                    kom[4] = komorki[0, 0];
                    kom[5] = komorki[w, 0];

                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w + 1, kol - 1];
                    kom[5] = komorki[w, kol - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w + 1, 0];
                    kom[5] = komorki[w, 0];

                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[wr - 1, k - 1];
                    kom[5] = komorki[wr - 1, k];

                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k - 1];
                    kom[1] = komorki[w - 1, k];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w, k - 1];
                    kom[4] = komorki[0, k];
                    kom[5] = komorki[0, k + 1];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w - 1, k - 1];
                    kom[5] = komorki[w, k - 1];
                }

            }

            return kom;

        }

        private Komorka[] sasiedztwoHP(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];

                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w - 1, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k - 1];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w - 1, k + 1];

                    kom[3] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w + 1, k - 1];

                    kom[2] = komorki[w, k - 1];

                    kom[3] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k];

                    kom[3] = komorki[w + 1, k - 1];

                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[4];

                    kom[0] = komorki[w - 1, k + 1];

                    kom[1] = komorki[w - 1, k];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w, k - 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[6];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w, k + 1];

                    kom[2] = komorki[w + 1, k - 1];

                    kom[3] = komorki[w + 1, k];

                    kom[4] = komorki[w, k - 1];

                    kom[5] = komorki[w - 1, k + 1];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[6];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, kol - 1];
                    kom[3] = komorki[wr - 1, k + 1];
                    kom[4] = komorki[wr - 1, k];
                    kom[5] = komorki[w, kol - 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[0, kol - 1];
                    kom[3] = komorki[0, 0];
                    kom[4] = komorki[w, kol - 1];
                    kom[5] = komorki[w - 1, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k - 1];
                    kom[3] = komorki[0, 0];
                    kom[4] = komorki[wr - 1, k];
                    kom[5] = komorki[wr - 1, 0];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, 0];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k];
                    kom[4] = komorki[0, k - 1];
                    kom[5] = komorki[w, 0];

                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w - 1, k + 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w - 1, kol - 1];
                    kom[5] = komorki[w, kol - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, 0];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w + 1, k - 1];
                    kom[5] = komorki[w, 0];

                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[w + 1, k - 1];
                    kom[4] = komorki[wr - 1, k + 1];
                    kom[5] = komorki[wr - 1, k];

                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k + 1];
                    kom[1] = komorki[w - 1, k];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w, k - 1];
                    kom[4] = komorki[0, k];
                    kom[5] = komorki[0, k - 1];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w, k + 1];
                    kom[2] = komorki[w + 1, k - 1];
                    kom[3] = komorki[w + 1, k];
                    kom[4] = komorki[w - 1, k + 1];
                    kom[5] = komorki[w, k - 1];
                }

            }

            return kom;

        }

        private Komorka[] sasiedztwoPL(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[1];

                    kom[0] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[1];

                    kom[0] = komorki[w - 1, k];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w + 1, k - 1];

                    kom[2] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k - 1];

                    kom[2] = komorki[w, k - 1];

                    kom[3] = komorki[w + 1, k - 1];

                    kom[4] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k - 1];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k - 1];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k - 1];

                    kom[1] = komorki[w - 1, k];

                    kom[2] = komorki[w, k - 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k - 1];

                    kom[3] = komorki[w, k - 1];

                    kom[4] = komorki[w - 1, k - 1];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[5];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w + 1, k];
                    kom[1] = komorki[w + 1, kol - 1];
                    kom[2] = komorki[wr - 1, kol - 1];
                    kom[3] = komorki[wr - 1, k];
                    kom[4] = komorki[w, kol - 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[0, 0];
                    kom[2] = komorki[0, kol - 1];
                    kom[3] = komorki[w, kol - 1];
                    kom[4] = komorki[w - 1, kol - 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k - 1];
                    kom[2] = komorki[w + 1, k];
                    kom[3] = komorki[wr - 1, k];
                    kom[4] = komorki[wr - 1, k - 1];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k - 1];
                    kom[4] = komorki[0, k];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, kol - 1];
                    kom[3] = komorki[w, kol - 1];
                    kom[4] = komorki[w - 1, kol - 1];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k - 1];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[w + 1, k - 1];
                    kom[4] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k - 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k - 1];
                    kom[3] = komorki[wr - 1, k - 1];
                    kom[4] = komorki[wr - 1, k];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k - 1];
                    kom[1] = komorki[w - 1, k];
                    kom[2] = komorki[w, k - 1];
                    kom[3] = komorki[0, k - 1];
                    kom[4] = komorki[0, k];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k - 1];
                    kom[3] = komorki[w, k - 1];
                    kom[4] = komorki[w - 1, k - 1];
                }

            }

            return kom;
        }

        private Komorka[] sasiedztwoPP(int w, int k)
        {
            Komorka[] kom;

            if (wbFlag == 0) //nieperiodyczne
            {
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k + 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom = new Komorka[1];

                    kom[0] = komorki[w + 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom = new Komorka[1];

                    kom[0] = komorki[w - 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w + 1, k + 1];

                    kom[4] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom = new Komorka[2];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w + 1, k];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w, k + 1];

                    kom[1] = komorki[w + 1, k];

                    kom[2] = komorki[w + 1, k + 1];

                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom = new Komorka[3];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];
                }
                else                                              //srodek
                {
                    kom = new Komorka[5];

                    kom[0] = komorki[w - 1, k];

                    kom[1] = komorki[w - 1, k + 1];

                    kom[2] = komorki[w, k + 1];

                    kom[3] = komorki[w + 1, k + 1];

                    kom[4] = komorki[w + 1, k];
                }
            }
            else //periodycznie
            {
                kom = new Komorka[5];
                if (w == 0 && k == 0) //lewy gorny
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[wr - 1, k];
                    kom[4] = komorki[wr - 1, k + 1];
                }
                else if (w == wr - 1 && k == 0) //lewy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[0, k + 1];
                    kom[4] = komorki[0, 0];
                }
                else if (w == 0 && k == kol - 1) //prawy górny
                {
                    kom[0] = komorki[w + 1, k];
                    kom[1] = komorki[w + 1, 0];
                    kom[2] = komorki[0, 0];
                    kom[3] = komorki[wr - 1, 0];
                    kom[4] = komorki[wr - 1, k];
                }
                else if (w == wr - 1 && k == kol - 1) //prawy dolny
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[0, k];
                    kom[2] = komorki[0, 0];
                    kom[3] = komorki[w, 0];
                    kom[4] = komorki[w - 1, 0];
                }
                else if ((w != 0 || w != wr - 1) && k == 0) //lewa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[w + 1, k];
                }
                else if ((w != 0 || w != wr - 1) && k == kol - 1) //prawa krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, 0];
                    kom[3] = komorki[w, 0];
                    kom[4] = komorki[w - 1, 0];
                }
                else if (w == 0 && (k != 0 || k != kol - 1)) //gorna krawedz
                {
                    kom[0] = komorki[w, k + 1];
                    kom[1] = komorki[w + 1, k];
                    kom[2] = komorki[w + 1, k + 1];
                    kom[3] = komorki[wr - 1, k];
                    kom[4] = komorki[wr - 1, k + 1];
                }
                else if (w == wr - 1 && (k != 0 || k != kol - 1)) //dolna krawedz
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[0, k];
                    kom[4] = komorki[0, k + 1];
                }
                else                                              //srodek
                {
                    kom[0] = komorki[w - 1, k];
                    kom[1] = komorki[w - 1, k + 1];
                    kom[2] = komorki[w, k + 1];
                    kom[3] = komorki[w + 1, k + 1];
                    kom[4] = komorki[w + 1, k];
                }

            }

            return kom;
        }

        private void iteracjaRozrostu(Komorka kom, int w, int k, Komorka[,] tp)
        {
            Random rnd = new Random();
            Random r = new Random();
            int stan, rand, los;
            Brush kol;
            Komorka[] komor = new Komorka[0]; //tablica zawiera sasiadow komorki w którch jest ziarno
            int licz = 0;
            Komorka komorka = kom;

            switch (sasFlag)
            {
                case 1:
                    komor = sasiedztwoVN(w, k);  //vonNeumann
                    break;
                case 2:
                    komor = sasiedztwoM(w, k); //Moore
                    break;
                case 3:
                    komor = sasiedztwoHL(w, k); //Moore
                    break;
                case 4:
                    komor = sasiedztwoHP(w, k); //Moore
                    break;
                case 5:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoHL(w, k);
                    else
                        komor = sasiedztwoHP(w, k);
                    break;
                case 6:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoPL(w, k);
                    else
                        komor = sasiedztwoPP(w, k);
                    break;
            }


            for (int i = 0; i < komor.Length; i++)
            {
                if (komor[i].getStan() != 0 && komor[i].getStan() != -1)
                    licz++;
            }
            if (licz != 0)
            {
                int result = GetBiggerAmount(komor);
                int index=0;
                 //rand = rnd.Next(0, komor.Length);

                /* while (komor[rand].getStan() == result)
                     rand = rnd.Next(0, komor.Length);*/
                 for(int i = 0; i < komor.Length; i++)
                {
                    if (komor[i].getStan() == result)
                    {
                        index = i;
                        break;
                    }
                }

                stan = komor[index].getStan();
                kol = komor[index].getKolor();



                tp[w, k] = new Komorka(255, komor[index].getRectangle());
                tp[w, k].setStan(stan);
                tp[w, k].setKolor(kol);
            }
        }

        private int GetBiggerAmount(Komorka [] neighbours)
        {
            int[,] tab= new int[8, 2] { {0,0}, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } } ;
            int stan, max=0,result=0;
            for(int i = 0; i < neighbours.Length;i++)
            {
                stan = neighbours[i].getStan();
                if (stan > 0) {
                    for (int j = 0; j < 8; j++)
                    {
                        if (tab[j, 0] == 0)
                        {
                            tab[j, 0] = stan;
                            tab[j, 1]++;
                            break;
                        }else if (tab[j, 0] == stan)
                        {
                            tab[j, 1]++;
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                if (max < tab[i, 1] && tab[i,1]>0)
                {
                    max = tab[i, 1];
                    result = tab[i,0];
                }
            }

            return result;
        }

        private void ustalenieGranic(Komorka[,] tab)
        {
            int los;
            Komorka[] sasiedzi = new Komorka[8];
            Random r = new Random();

            for (int i = 0; i < wr; ++i)
            {
                for (int j = 0; j < kol; ++j)
                {
                    switch (sprawdzSasiedztwo())
                    {
                        case 1:
                            sasiedzi = sasiedztwoVN(i, j);  //vonNeumann
                            break;
                        case 2:
                            sasiedzi = sasiedztwoM(i, j); //Moore
                            break;
                        case 3:
                            sasiedzi = sasiedztwoHL(i, j); //Moore
                            break;
                        case 4:
                            sasiedzi = sasiedztwoHP(i, j); //Moore
                            break;
                        case 5:
                            los = r.Next(0, 2);
                            if (los == 0)
                                sasiedzi = sasiedztwoHL(i, j);
                            else
                                sasiedzi = sasiedztwoHP(i, j);
                            break;
                        case 6:
                            los = r.Next(0, 2);
                            if (los == 0)
                                sasiedzi = sasiedztwoPL(i, j);
                            else
                                sasiedzi = sasiedztwoPP(i, j);
                            break;
                    }

                    for (int k = 0; k < sasiedzi.Length; ++k)
                    {
                        if (sasiedzi[k].getStan() != tab[i, j].getStan())
                        {
                            tab[i, j].setGranica(true);
                            break;
                        }
                    }
                }
            }


        }

        private void rekrystalizacja2(Random rg)
        {
            double dr, dr2;
            double sumaR;
            Random r1 = new Random();
            Random r2 = new Random();
            Komorka[] tabKom = new Komorka[wspk + 1];

            for (int i = 1; i < ro.Length; ++i)
            {
                sumaR = 0;

                dr = ro[i] - ro[i - 1];

                dr = dr / (wr * kol);
                dr2 = dr;

                tabKom[0] = randomKomorka(randomglobal, randomglobal);

                if (tabKom[0].getGranica() == true)
                {
                    dr = dr * 0.8;
                    sumaR += dr2 * 0.2;
                }
                else
                {
                    dr = dr * 0.2;
                    sumaR += dr2 * 0.8;
                }

                tabKom[0].dodajRo(dr);

                for (int j = 1; j < tabKom.Length; ++j)
                {
                    tabKom[j] = randomKomorka(r1, r2);
                    tabKom[j].dodajRo(sumaR / wspk);
                }

                sprawdzCzyPowstanieNoweZiarno(tabKom);

                if (liczbaPorekrystalizacji >= wr * kol)
                    break;
            }
        }

        private Komorka randomKomorka(Random r1, Random r2)
        {
            int[] wsp = new int[2];

            int x, y;
            Random r = new Random();
            x = r.Next(0, wr);
            y = r.Next(0, kol);
            while (komorki[x, y].getBu() == true)
            {
                x = r.Next(0, wr);
                y = r.Next(0, kol);
            }
            komorki[x, y].x = x;
            komorki[x, y].y = y;
            return komorki[x, y];
        }

        private Komorka randomKomorka2(Random r1, Random r2)
        {
            int[] wsp = new int[2];

            int x, y;
            Random r = new Random();
            x = r.Next(0, wr);
            y = r.Next(0, kol);



            komorki[x, y].x = x;
            komorki[x, y].y = y;
            return komorki[x, y];
        }

        private void sprawdzCzyPowstanieNoweZiarno(Komorka[] tab)
        {
            for (int i = 0; i < tab.Length; ++i)
            {
                if (tab[i].getRo() > kro && tab[i].getBu() == false)
                {
                    stworzZiarno(tab[i]);
                }
            }
        }

        private void stworzZiarno(Komorka k)
        {
            Random r = new Random();
            this.Dispatcher.Invoke(new Action(() => k.ustawStaniKolor(r.Next(0, 50), r)));
            liczbaPorekrystalizacji++;
            k.setBu(true);
            rozrostNowegoZiarna(k);
        }

        private void rozrostNowegoZiarna(Komorka k)
        {
            Komorka[] komor = new Komorka[0];
            int x = k.x;
            int y = k.y;

            Random r = new Random();
            int los = 0;

            switch (sasFlag)
            {
                case 1:
                    komor = sasiedztwoVN(x, y);  //vonNeumann
                    break;
                case 2:
                    komor = sasiedztwoM(x, y); //Moore
                    break;
                case 3:
                    komor = sasiedztwoHL(x, y); //Moore
                    break;
                case 4:
                    komor = sasiedztwoHP(x, y); //Moore
                    break;
                case 5:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoHL(x, y);
                    else
                        komor = sasiedztwoHP(x, y);
                    break;
                case 6:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoPL(x, y);
                    else
                        komor = sasiedztwoPP(x, y);
                    break;
            }

            for (int i = 0; i < komor.Length; ++i)
            {
                if (komor[i].getRo() < k.getRo() && komor[i].getBu() == false && komor[i].getStan() != -1)
                {
                    komor[i].setStan(k.getStan());
                    komor[i].setKolor(k.getKolor());
                    komor[i].setBu(true);
                    komor[i].update();
                    ++liczbaPorekrystalizacji;
                }
            }
        }

        private void MonteCarlo(Random r, Random r2)
        {
            // Random r = new Random();
            //Random r2 = new Random();

            Komorka k = null;
            Komorka[] otoczenie = new Komorka[0];
<<<<<<< HEAD

            Brush kolor = null;
            int E = 0, E2 = 0;
            int deltaE = 0, iteracja = 0;
            //   int x, y;

=======

            Brush kolor = null;
            int E = 0, E2 = 0;
            int deltaE = 0, iteracja = 0;
            //   int x, y;

>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
            while (E == 0)
            {
                k = randomKomorka2(r, r2);

                otoczenie = zbierzSasiadow(k.x, k.y);

                E = obliczEnergie(k, otoczenie);
            }


            for (int i = 0; i < stany.Length; ++i)
            {

                k.setStan(stany[i]);

                E2 = obliczEnergie(k, otoczenie);
                iteracja = i;
                if (E2 <= E)
                {
                    i = stany.Length;
                }
            }


            for (int j = 0; j < otoczenie.Length; ++j)
            {
                if (otoczenie[j].getStan() == k.getStan())
                {
                    kolor = otoczenie[j].getKolor();
                    j = 9;
                }

            }


            //   if (kolor != null)
            //  {
            k.setStan(stany[iteracja]);
            k.setKolor(kolor);
            k.update();
            //   }
        }

        private void stworzNowyRandom(Random r)
        {
            r = new Random();
        }

        private Komorka[] zbierzSasiadow(int x, int y)
        {
            Komorka[] komor = new Komorka[0];
            Random r = new Random();
            int los = 0;

            switch (sprawdzSasiedztwo())
            {
                case 1:
                    komor = sasiedztwoVN(x, y);  //vonNeumann
                    break;
                case 2:
                    komor = sasiedztwoM(x, y); //Moore
                    break;
                case 3:
                    komor = sasiedztwoHL(x, y); //Moore
                    break;
                case 4:
                    komor = sasiedztwoHP(x, y); //Moore
                    break;
                case 5:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoHL(x, y);
                    else
                        komor = sasiedztwoHP(x, y);
                    break;
                case 6:
                    los = r.Next(0, 2);
                    if (los == 0)
                        komor = sasiedztwoPL(x, y);
                    else
                        komor = sasiedztwoPP(x, y);
                    break;
            }
            return komor;
        }

        private int obliczEnergie(Komorka k, Komorka[] tab)
        {
            int E = 0;
            for (int i = 0; i < tab.Length; ++i)
            {
                if (tab[i].getStan() != k.getStan() && tab[i].getStan() != -1)
                    ++E;
            }
            return E;
        }
        private void clearTab(int[] s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                s[i] = 0;
            }
        }

        private void clearKomorki(Komorka[] k)
        {
            for (int i = 0; i < k.Length; i++)
            {
                k[i] = null;
            }
        }

        private int petla()
        {
            Komorka[,] tabPrzejscia = new Komorka[wr, kol];
            brakPustych = true;
            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getStan() == 0)
                    {
                        iteracjaRozrostu(komorki[i, j], i, j, tabPrzejscia);
                        brakPustych = false;
                    }
                    else
                        tabPrzejscia[i, j] = komorki[i, j];
                }
            }
            if (brakPustych == false)
            {
                this.Dispatcher.Invoke(new Action(() => rozrost(tabPrzejscia, komorki)));
                return 0;
            }
            else
                return 1;
        }

        private void rozrost(Komorka[,] tp, Komorka[,] kom)
        {
            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (tp[i, j] != null && tp[i, j].getStan() != kom[i, j].getStan())
                    {
                        kom[i, j].setStan(tp[i, j].getStan());
                        kom[i, j].setKolor(tp[i, j].getKolor());
                        this.Dispatcher.Invoke(new Action(() => kom[i, j].update()));
                        //Thread.Sleep(500);

                    }
                    kom[i, j].update();
                }

            }
        }

        private int sprawdzLosowanieZiarna()
        {
            int flag = 0;

            losowanie = (ComboBoxItem)losCB.SelectedItem;
            if (losowanie == cbi1)
                flag = 1;
            else if (losowanie == cbi2)
                flag = 2;
            else if (losowanie == cbi3)
                flag = 3;
            return flag;
        }

        private int sprawdzSasiedztwo()
        {
            int flag = 0;

            losowanie = (ComboBoxItem)sasCB.SelectedItem;

            if (losowanie == sascbi1)
                flag = 1;
            else if (losowanie == sascbi2)
                flag = 2;
            else if (losowanie == sascbi3)
                flag = 3;
            else if (losowanie == sascbi4)
                flag = 4;
            else if (losowanie == sascbi5)
                flag = 5;
            else if (losowanie == sascbi6)
                flag = 6;
            return flag;
        }

        private void losoweZiarna(int il, Random rnd)
        {
            int k = 0, w = 0, x = 0, flag = 0;
            for (int i = 0; i < il; i++)
            {
                do
                {
                    flag = 0;
                    k = rnd.Next(0, kol);
                    w = rnd.Next(0, wr);
<<<<<<< HEAD
                    x = rnd.Next(1, 50);
=======
                    x = rnd.Next(1, 51);
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
                    if (komorki[w, k].getStan() == -1)
                        flag = 1;
                    else
                    {
                        stany[i] = x;
                        komorki[w, k].ustawStaniKolor(x, rnd);
                    }
                } while (flag == 1);
            }
        }

        private void rownomierneZiarna(int il)
        {
            int wsp = wr * kol / il;
            int k = 0, w = 0, x = 0;
            Random rnd = new Random();
            for (int i = 0; i < il; i++)
            {
                x = rnd.Next(1, 51);
                stany[i] = x;
                if (i == 1)
                {
                    w = wr - 1;
                    k = kol - 1;
                }
                komorki[w, k].ustawStaniKolor(x, rnd);
                if (i == 1)
                {
                    w = 0;
                    k = 0;
                }
                k += wsp + 1;
                if (k >= kol)
                {
                    k = k - kol;
                    w++;

                }
            }
        }

        private void promienZiarna(int il, int r)
        {
            int dodano = 0;
            int k = 0, w = 0, x = 0;

            int l, p, d, g;
            Random rnd = new Random();

            int[,] tab = new int[wr, kol];
            for (int i = 0; i < wr; ++i)
                for (int j = 0; j < kol; ++j)
                    tab[i, j] = 0;
            while (dodano < il)
            {
                k = rnd.Next(0, kol);
                w = rnd.Next(0, wr);
                x = rnd.Next(1, 51);
                stany[dodano] = x;
                if (tab[w, k] == 0)
                {
                    komorki[w, k].ustawStaniKolor(x, rnd);
                    if ((w - r) < 0)
                        g = 0;
                    else
                        g = w - r;
                    if ((w + r) >= wr)
                        d = wr - 1;
                    else
                        d = w + r;
                    if ((k - r) < 0)
                        l = 0;
                    else
                        l = k - r;
                    if ((k + r) >= wr)
                        p = wr - 1;
                    else
                        p = k + r;

                    for (int i = g; i <= d; ++i)
                        for (int j = l; j <= p; ++j)
                            tab[i, j] = 1;

                    ++dodano;
                }
            }
        }

        private void utworzZiarno(int w, int k)
        {
            int x = 0;
            Random rnd = new Random();
            x = rnd.Next(1, 51);
            komorki[w, k].ustawStaniKolor(x, rnd);
        }

        private void planszaB_Click(object sender, RoutedEventArgs e)
        {
            wr = Int32.Parse(lWr.Text);
            kol = Int32.Parse(lKol.Text);
            utworzPlansze(kol, wr);
            utworzKomorki(kol, wr, plansza);


        }

        private void startB_Click(object sender, RoutedEventArgs e)
        {

            flag = 0;
            tflag = 1;
            sasFlag = sprawdzSasiedztwo();
            int stopFlag, counter = 0; ;

            if (periodRB.IsChecked == true)
                wbFlag = 1;
            if (nieperRB.IsChecked == true)
                wbFlag = 0;

            do
            {
                stopFlag = petla();
               counter++;
            } while (stopFlag == 0 && counter <= 1000);
            /* if (t == null)
             {
                 this.t = new Thread(this.Run);
             }
             try
             {
                 t.Start();
             }
             catch (Exception exc)
             {

             }
             // petla();*/

        }

        private void stop()
        {
            flag = 1;
            t.Abort();
            brakPustych = true;
        }

        /*  private void odczytPliku(string path)
          {
              int[] tab = new int[3];
              int i,j,s;
              string str;
              FileInfo f;
              FileStream fs;
              f = new FileInfo(path);

              fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                  using (StreamReader sr = new StreamReader(fs))
                  {
                      while (!sr.EndOfStream)
                      {
                          str=sr.ReadLine().ToString();
                          string[] strTab=str.Split(' ');
                      i = Int32.Parse(strTab[0]);
                      j = Int32.Parse(strTab[1]);
                      s = Int32.Parse(strTab[2]);
                      komorki[i,j].setStan(s);
                    //  this.Dispatcher.Invoke(new Action(() => komorki[i, j].update()));
                      }
                  }
              kolorujZiarno();
          }*/

        private static void kolorujZiarno(Komorka k, byte r, byte g, byte b)
        {
            Brush kolor;
            Random rnd = new Random();
            kolor = new SolidColorBrush(Color.FromRgb(r, g, b));
            k.setKolor(kolor);
            k.update();
        }

        private string zapisDoPliku(Komorka[,] kom)
        {
            FileInfo f;
            FileStream fs;
            string fileName = "rek.txt";
            string path = System.IO.Path.Combine(Environment.CurrentDirectory, fileName);
            f = new FileInfo(path);
            string s;

            fs = new FileStream(path, FileMode.Open, FileAccess.Write);
            using (StreamWriter sw = new StreamWriter(fs))
            {
                for (int i = 0; i < wr; i++)
                {
                    for (int j = 0; j < kol; j++)
                    {
                        s = i + " " + j + " " + kom[i, j].getStan() + " " + kom[i, j].getKolorStr();
                        sw.WriteLine(s);
                    }
                }
            }
            return path;
        }

        private void odczytPliku(string path)
        {
            int[] tab = new int[3];
            int i, j, s;
            string str;
            FileInfo f;
            FileStream fs;
            f = new FileInfo(path);

            fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            using (StreamReader sr = new StreamReader(fs))
            {
                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine().ToString();
                    string[] strTab = str.Split(' ');
                    i = Int32.Parse(strTab[0]);
                    j = Int32.Parse(strTab[1]);
                    s = Int32.Parse(strTab[2]);
                    komorki[i, j].setStan(s);
                    komorki[i, j].setKolorStr(strTab[3]);
                }
            }
        }

        private void stopB_Click(object sender, RoutedEventArgs e)
        {
            stop();
        }

        private void dodajB_Click(object sender, RoutedEventArgs e)
        {

            liczbaZiaren = Int32.Parse(ilZiartb.Text);
            stany = new int[liczbaZiaren];

            losFlag = sprawdzLosowanieZiarna();
            switch (losFlag)
            {
                case 1:
                    losoweZiarna(liczbaZiaren, randomglobal);
                    break;
                case 2:
                    rownomierneZiarna(liczbaZiaren);
                    break;
                    /*  case 3:
                          int r = Int32.Parse(rTB.Text);
                          promienZiarna(liczbaZiaren,r);
                          break;*/
            }
        }

        private void rekB_Click(object sender, RoutedEventArgs e)
        {
            tflag = 2;
            flag = 0;
            wspk = Int32.Parse(kTB.Text);
            ustalenieGranic(komorki);
            ro = new double[200];
            // ro = odczytPliku();
            kro = ro[70] / (wr * kol);
            t = new Thread(this.Run);
            t.Start();
            /* if(liczbaPorekrystalizacji<=wr*kol)
                  rekrystalizacja2(randomglobal);*/


        }

        private void mcB_Click(object sender, RoutedEventArgs e)
        {
            /*liczMC = 0;
            tflag = 3;
            randomglobal2 = new Random();
            flag = 0;
            t = new Thread(this.Run);
<<<<<<< HEAD
            t.Start();*/
            //  MonteCarlo();
            MCIterations = Int32.Parse(MCItb.Text);
            grainBoundariesEn = Int32.Parse(gbeTB.Text);
            MonteCarlo MC = new MonteCarlo(komorki,kol,wr,grainBoundariesEn,MCIterations,this);
            for(int i = 0; i < MCIterations; i++)
            {
                komorki=MC.MCIteration();
                foreach (Komorka k in komorki)
                    k.update();
            }
=======
            t.Start();
            //  MonteCarlo();
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
        }

        private void ExportClick(object sender, RoutedEventArgs e)
        {
            zapisDoPliku(komorki);
        }

        private void ImportClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "Text file (*.txt)| *.txt;|All files (*.*)|*.*",
                InitialDirectory = Environment.CurrentDirectory
            };

            if (dlg.ShowDialog() == true)
            {
                odczytPliku(dlg.FileName);
            }

        }

        private void bordersB_Click(object sender, RoutedEventArgs e)
        {
            ustalenieGranic(komorki);
<<<<<<< HEAD

            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getGranica() == false)
                    {
                        komorki[i, j].setStan(0);
                        kolorujZiarno(komorki[i, j], 255, 255, 255);
                    }
                    else
                    {
                        komorki[i, j].setStan(-1);
                        kolorujZiarno(komorki[i, j], 0, 0, 0);
                    }
                }
            }
        }

        private void InclusionsB_Click(object sender, RoutedEventArgs e)
        {
            int inclNum = Int32.Parse(numOfInclTB.Text);
            int inclSize = Int32.Parse(inclSizeTB.Text);
            wr = Int32.Parse(lWr.Text);
            kol = Int32.Parse(lKol.Text);
            Random rnd = new Random();
            int flag, odl, odl2;
            int c = 0, r = 0;
            if (inclCB.SelectedIndex == 0)
            {
                for (int i = 0; i < inclNum; i++)
                {
                    c = rnd.Next(0, kol);
                    r = rnd.Next(0, wr);
                    for (int j = c; j < c + inclSize; j++)
                    {
                        for (int k = r; k < r + inclSize; k++)
                        {
                            if (k < wr && j < kol)
                            {
                                komorki[k, j].ustawStan(-1);
                                kolorujZiarno(komorki[k, j], 0, 0, 0);
                            }
                        }
                    }

                }
            }
            else
            {
                for (int i = 0; i < inclNum; i++)
                {
                        c = rnd.Next(0, kol);
                        r = rnd.Next(0, wr);
                   // odl = countDistance(c + (inclSize / 2), r + (inclSize / 2), inclSize / 2);
                    for (int j = c-inclSize/2; j <= c + inclSize/2; j++)
                    {
                        for (int k = r-inclSize/2; k <= r + inclSize/2; k++)
                        {
                            odl2 = countCircle(c,r,j, k, inclSize/2);
                            if (k < wr && j < kol)
                            {
                                //if (((int)Math.Pow(j - (c + 1 + inclSize / 2), 2) + (int)Math.Pow(k - (r + inclSize / 2), 2)) < inclSize * inclSize)
                                if (odl2<=(int)Math.Pow(inclSize/2,2))
                                {
                                    if (k < kol && j < wr && k>=0 && j>=0)
                                    {
                                        komorki[k, j].ustawStan(-1);
                                        kolorujZiarno(komorki[k, j], 0, 0, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        int countDistance(int col, int row, int size)
        {
            int result;

            result = (int)Math.Sqrt(Math.Pow(row - col, 2) + Math.Pow((row + size) - (col + size), 2));

            return result;
        }

        int countCircle(int a,int b,int col, int row, int size)
        {
            int result;

            result = (int)(Math.Pow(row - b, 2) + Math.Pow((col - a), 2));

            return result;
        }

        void MarkOneGrain(Rectangle obj)
        {
            ustalenieGranic(komorki);

            int stan = 0;
=======
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47

            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
<<<<<<< HEAD
                    if (komorki[i, j].getRectangle() == obj)
                    {
                        stan = komorki[i, j].getStan();
                        break;
                    }
                }
            }
            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getStan() != stan)
                    {
                        kolorujZiarno(komorki[i, j], 255, 255, 255);
                        komorki[i, j].setStan(0);
                    }
                    else
                    {
                        if (komorki[i, j].getGranica() == true)
                        {
                            komorki[i, j].setStan(-1);
                            kolorujZiarno(komorki[i, j], 0, 0, 0);
                        }
                        else
                        {
                            kolorujZiarno(komorki[i, j], 255, 255, 255);
                            komorki[i, j].setStan(0);
                        }
                    }


                }
            }
        }

        void CreateGrain(int i, int j)
        {
            Random rnd = new Random();
            int x = rnd.Next(0, 51);
            komorki[i, j].ustawStaniKolor(x, rnd);
        }

        private void selectGrainsB_Click(object sender, RoutedEventArgs e)
        {
            int x, y,flag,counter=0;
            int numOfGrains = Int32.Parse(numOfGrainsTB.Text);
            int[] states = new int[numOfGrains];
            Random rnd = new Random(),rnd2 = new Random();

            for (int i = 0; i < states.Length; i++)
                states[i] = 0;

            for(int i = 0; i < numOfGrains; i++)
            {
                counter = 0;
                do
                {
                    counter++;
                    flag = 0;
                    x = rnd.Next(0, kol);
                    y = rnd2.Next(0, wr);
                    if (komorki[x, y].getStan() > 0)
                    {
                        for (int j = 0; j < states.Length; j++)
                        {
                            if (states[j] == komorki[x, y].getStan())
                            {
                                flag = 1;
                                break;                            
                            }
                            else if (states[j] == 0)
                            {
                                states[j] = komorki[x, y].getStan();
                                break;                                
                            }
                        }
                    }
                } while (flag == 1 && counter < kol*wr);
            }
            PrepareStructure(states);
        }

        private void MCPrepareB_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            Brush[] colors = randomColors(rnd);
            for(int i = 0; i < kol; i++)
            {
                for(int j = 0; j < wr; j++)
                {
                    if (komorki[i, j].getStan() >= 0)
                    {
                        int x = rnd.Next(1, 50);
                        komorki[i, j].ustawStan(x);
                        komorki[i, j].setKolor(colors[x]);
                        komorki[i, j].update();
                    }
=======
                    if (komorki[i, j].getGranica() == false)
                    {
                        komorki[i, j].setStan(0);
                        kolorujZiarno(komorki[i, j], 255, 255, 255);
                    }
                    else
                    {
                        komorki[i, j].setStan(-1);
                        kolorujZiarno(komorki[i, j], 0, 0, 0);
                    }
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
                }
            }
        }

<<<<<<< HEAD
        private Brush[] randomColors(Random rnd)
        {
            Brush[] colors = new Brush[50];
            for (int i = 0; i < 50; i++)
            {         
                SolidColorBrush kolor = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256)));
                colors[i] = (Brush)kolor;
            }
            return colors;
        }
=======
        private void InclusionsB_Click(object sender, RoutedEventArgs e)
        {
            int inclNum = Int32.Parse(numOfInclTB.Text);
            int inclSize = Int32.Parse(inclSizeTB.Text);
            wr = Int32.Parse(lWr.Text);
            kol = Int32.Parse(lKol.Text);
            Random rnd = new Random();
            int flag, odl, odl2;
            int c = 0, r = 0;
            if (inclCB.SelectedIndex == 0)
            {
                for (int i = 0; i < inclNum; i++)
                {
                    flag = 0;
                    do
                    {
                        c = rnd.Next(0, kol);
                        r = rnd.Next(0, wr);
                        for (int j = c; j < c + inclSize; j++)
                        {
                            for (int k = r; k < r + inclSize; k++)
                            {
                                if (k < wr && j < kol)
                                {
                                    if (komorki[k, j].getStan() == -1)
                                    {
                                        flag = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    } while (flag == 1);

                    for (int j = c; j < c + inclSize; j++)
                    {
                        for (int k = r; k < r + inclSize; k++)
                        {
                            if (k < wr && j < kol)
                            {
                                komorki[k, j].ustawStan(-1);
                                kolorujZiarno(komorki[k, j], 0, 0, 0);
                            }
                        }
                    }

                }
            }
            else
            {
                for (int i = 0; i < inclNum; i++)
                {
                    flag = 0;
                    do
                    {
                        c = rnd.Next(0, kol);
                        r = rnd.Next(0, wr);
                        for (int j = c; j < c + inclSize; j++)
                        {
                            for (int k = r; k < r + inclSize; k++)
                            {
                                if (k < wr && j < kol)
                                {
                                    if (komorki[k, j].getStan() == -1)
                                    {
                                        flag = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    } while (flag == 1);
                    odl = countDistance(c + (inclSize / 2), r + (inclSize / 2), inclSize / 2);
                    for (int j = c; j < c + inclSize; j++)
                    {
                        for (int k = r; k < r + inclSize; k++)
                        {
                            odl2 = countDistance(j, k, inclSize / 2);
                            if (k < wr && j < kol)
                            {
                                if (((int)Math.Pow(j - (c + 1 + inclSize / 2), 2) + (int)Math.Pow(k - (r + inclSize / 2), 2)) < inclSize * inclSize)
                                {
                                    komorki[k, j].ustawStan(-1);
                                    kolorujZiarno(komorki[k, j], 0, 0, 0);
                                }
                            }
                        }
                    }
                    /*for (int j = c- inclSize; j < c; j++)
                    {
                        for (int k = r - inclSize; k < r ; k++)
                        {
                            odl2 = countDistance(j, k, inclSize);
                            if (k < wr && j < kol && odl2 <= odl)
                            {
                                komorki[k, j].ustawStan(-1);
                                kolorujZiarno(komorki[k, j], 0, 0, 0);
                            }
                        }
                    }*/

                }
            }
        }

        int countDistance(int col, int row, int size)
        {
            int result;

            result = (int)Math.Sqrt(Math.Pow(row - col, 2) + Math.Pow((row + size) - (col + size), 2));

            return result;
        }

        int countCircle(int col, int row, int size)
        {
            int result;

            result = (int)(Math.Pow(row - col, 2) + Math.Pow((row + size) - (col + size), 2));

            return result;
        }

        void MarkOneGrain(Rectangle obj)
        {
            ustalenieGranic(komorki);

            int stan = 0;

            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getRectangle() == obj)
                    {
                        stan = komorki[i, j].getStan();
                        break;
                    }
                }
            }
            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getStan() != stan)
                    {
                        kolorujZiarno(komorki[i, j], 255, 255, 255);
                        komorki[i, j].setStan(0);
                    }
                    else
                    {
                        if (komorki[i, j].getGranica() == true)
                        {
                            komorki[i, j].setStan(-1);
                            kolorujZiarno(komorki[i, j], 0, 0, 0);
                        }
                        else
                        {
                            kolorujZiarno(komorki[i, j], 255, 255, 255);
                            komorki[i, j].setStan(0);
                        }
                    }

>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47

        private void DistributeEnergyB_Click(object sender, RoutedEventArgs e)
        {
            int typeOfDistribution;
            EnergyDistribution ed = new EnergyDistribution(komorki, kol, wr, Int32.Parse(enInsideTB.Text), Int32.Parse(enBordersTB.Text));
            typeOfDistribution = enDistributionCB.SelectedIndex;
            
            switch (typeOfDistribution)
            {
                case 0:
                    ustalenieGranic(komorki);
                    ed.HeteroDistribution();
                    break;
                case 1:
                    ed.HomoDistribution();
                    break;
            }
        }

        private void PrepareStructure(int []states)
        {
            int[] tab = states;
            for(int i = 0; i < kol; i++)
            {
                for(int j = 0; j < wr; j++)
                {

                        for(int k = 0; k < tab.Length; k++)
                        {
                            if (komorki[i, j].getStan() == tab[k] && tab[k]!=0)
                            {
                                komorki[i, j].setStan(-1);
                                if (substructureCB.SelectedIndex == 1)
                                    kolorujZiarno(komorki[i, j], 255, 0, 255);
                                break;
                            }
                        }
                    if (komorki[i, j].getStan() != -1)
                    {
                        komorki[i, j].setStan(0);
                        kolorujZiarno(komorki[i, j], 255, 255, 255);
                    }
                }
            }
        }

<<<<<<< HEAD
        void rectangleClick(object sender, MouseButtonEventArgs e)
        {
=======
        void CreateGrain(int i, int j)
        {
            Random rnd = new Random();
            int x = rnd.Next(0, 51);
            komorki[i, j].ustawStaniKolor(x, rnd);
        }

        void rectangleClick(object sender, MouseButtonEventArgs e)
        {
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
            Rectangle obj = (Rectangle)sender;
            for (int i = 0; i < wr; i++)
            {
                for (int j = 0; j < kol; j++)
                {
                    if (komorki[i, j].getRectangle() == obj && komorki[i, j].getStan() != 0)
                    {
                        MarkOneGrain(obj);
                    }
                    else if(komorki[i, j].getRectangle() == obj && komorki[i, j].getStan() == 0)
                        CreateGrain(i, j);

                }
<<<<<<< HEAD
            }           
=======
            }
            
           
>>>>>>> 1c5f58e9520485ab188c9b779459818b30c82b47
        }

    
    }
}

