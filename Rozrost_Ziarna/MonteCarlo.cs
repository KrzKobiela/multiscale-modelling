﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Rozrost_Ziarna
{
    class MonteCarlo
    {
        Komorka[,] tab, tempTab;
        Komorka[] neighbours;
        int col, row, grainBoundaryEn, MCiterations;
        Random rndCol, rndRow;
        MainWindow mw;
        List <int> colNum = new List<int>();
        List <int> rowNum = new List<int>();

        public MonteCarlo(Komorka[,] komorki, int col, int row, int gben, int mcit, MainWindow mw)
        {
            tab = komorki;
            this.col = col;
            this.row = row;
            grainBoundaryEn = gben;
            MCiterations = mcit;
            this.mw = mw;
        }

        public Komorka[,] MCIteration()
        {
            rndCol = new Random();
            rndRow = new Random();
            int rand,c,r;
            int state,flag;
            for (int i = 0; i < col * row; i++)
            {
                do
                {
                    flag = 0;
                    rand = rndCol.Next(0, col * row);
                    // r = rndRow.Next(0, row);
                    if (colNum.Count < col * row)
                    {
                        foreach (int num in colNum)
                        {
                            if (num == rand)
                            {
                                flag = 1;
                                break;
                            }
                            else
                            {
                                colNum.Add(rand);
                            }

                        }
                    }
                } while (flag == 1);
                c = rand / row;
                r = rand % row;
                state = tab[c, r].getStan();
                if (state != -1)
                {
                    neighbours = sasiedztwoM(r, c, tab);
                    int enBefore = CountEnergy(tab[c, r]);
                    changeState(tab[c, r], neighbours);
                    int enAfter = CountEnergy(tab[c, r]);
                    if (enAfter > enBefore)
                        tab[c, r].setStan(state);
                    else
                    {
                        setCellColor(tab[c, r]);
                        //     mw.Dispatcher.Invoke(new Action(() => tab[c, r].update()));
                        //tab[c,r].update();
                    }
                }
            }
            return tab;
        }

        private int CountEnergy(Komorka kom)
        {
            int en = 0;

            int stan = kom.getStan();

            foreach (Komorka k in neighbours)
                if (k.getStan() != stan)
                    en++;
            en *= grainBoundaryEn;
            return en;
        }
        private Komorka[] sasiedztwoM(int w, int k, Komorka[,] komorki)
        {
            Komorka[] kom;
            if (w == 0 && k == 0) //lewy gorny
            {
                kom = new Komorka[3];

                kom[0] = komorki[w, k + 1];

                kom[1] = komorki[w + 1, k];

                kom[2] = komorki[w + 1, k + 1];
            }
            else if (w == row - 1 && k == 0) //lewy dolny
            {
                kom = new Komorka[3];

                kom[0] = komorki[w - 1, k];

                kom[1] = komorki[w - 1, k + 1];

                kom[2] = komorki[w, k + 1];
            }
            else if (w == 0 && k == col - 1) //prawy górny
            {
                kom = new Komorka[3];

                kom[0] = komorki[w, k - 1];

                kom[1] = komorki[w + 1, k - 1];

                kom[2] = komorki[w + 1, k];
            }
            else if (w == row - 1 && k == col - 1) //prawy dolny
            {
                kom = new Komorka[3];

                kom[0] = komorki[w - 1, k];

                kom[1] = komorki[w - 1, k - 1];

                kom[2] = komorki[w, k - 1];
            }
            else if ((w != 0 || w != row - 1) && k == 0) //lewa krawedz
            {
                kom = new Komorka[5];

                kom[0] = komorki[w - 1, k];

                kom[1] = komorki[w - 1, k + 1];

                kom[2] = komorki[w, k + 1];

                kom[3] = komorki[w + 1, k + 1];

                kom[4] = komorki[w + 1, k];
            }
            else if ((w != 0 || w != row - 1) && k == col - 1) //prawa krawedz
            {
                kom = new Komorka[5];

                kom[0] = komorki[w - 1, k];

                kom[1] = komorki[w - 1, k - 1];

                kom[2] = komorki[w, k - 1];

                kom[3] = komorki[w + 1, k - 1];

                kom[4] = komorki[w + 1, k];
            }
            else if (w == 0 && (k != 0 || k != col - 1)) //gorna krawedz
            {
                kom = new Komorka[5];

                kom[0] = komorki[w, k - 1];

                kom[1] = komorki[w, k + 1];

                kom[2] = komorki[w + 1, k];

                kom[3] = komorki[w + 1, k + 1];

                kom[4] = komorki[w + 1, k - 1];
            }
            else if (w == row - 1 && (k != 0 || k != col - 1)) //dolna krawedz
            {
                kom = new Komorka[5];

                kom[0] = komorki[w - 1, k - 1];

                kom[1] = komorki[w - 1, k];

                kom[2] = komorki[w - 1, k + 1];

                kom[3] = komorki[w, k + 1];

                kom[4] = komorki[w, k - 1];
            }
            else                                              //srodek
            {
                kom = new Komorka[8];

                kom[0] = komorki[w - 1, k];

                kom[1] = komorki[w - 1, k + 1];

                kom[2] = komorki[w, k + 1];

                kom[3] = komorki[w + 1, k + 1];

                kom[4] = komorki[w + 1, k];

                kom[5] = komorki[w + 1, k - 1];

                kom[6] = komorki[w, k - 1];

                kom[7] = komorki[w - 1, k - 1];

            }
            return kom;
        }

        private void changeState(Komorka k,Komorka[] neighbours)
        {
            Random rnd = new Random();
            int stateold,index;

                stateold = k.getStan();
            do
            {
                index = (rnd.Next(0, neighbours.Length));

            } while (neighbours[index].getStan() < 0);
                k.setStan(neighbours[index].getStan());
               /* if (stateold != k.getStan())
                    break;*/

        }

        private void setCellColor(Komorka k)
        {
            foreach (Komorka komorka in tab)
            {
                if (komorka.getStan() == k.getStan())
                {
                    k.setKolor(komorka.getKolor());
                    break;
                }
            }
        }

        private void resetcounter(int[,] tab)
        {
            for (int i = 0; i < col; i++)
                for (int j = 0; j < row; j++)
                    tab[i, j] = 0;
               
        }
    }
}
