﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Rozrost_Ziarna{
    class Komorka
    {
        private int stan;
        private Rectangle rectangle;
        private Brush kolor;
        private Boolean granica;
        private double ro;
        private Boolean bu;
        public int x=0;
        public int y = 0;
        public int H;
        public int En;


        public Komorka(int kol, Rectangle pl)
        {
            byte k = (byte)kol;
            stan = 0;
            ro = 0;
            bu = false;
            granica = false;
            kolor= new SolidColorBrush(Color.FromRgb(k, k, k));
            rectangle = pl;
            update();
        }

        public void ustawStan(int s)
        {
            stan = s;
            update();
        }

        public void ustawStaniKolor(int s,Random rnd)
        {
            stan = s;
            kolor = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256)));
            update();
        }



        public void update()
        {
            rectangle.Fill = kolor;
        }
        public Brush getKolor()
        {

            return kolor;
        }

        public void setEnergy(int e)
        {
            En = e;
        }
        public int getEnergy()
        {
            return En;
        }

        public void setKolor(Brush c)
        {

            kolor = c;
        }
        public string getKolorStr()
        {
            string s = kolor.ToString();
            return s;
        }
        public void setKolorStr(string s)
        {
            Brush b = (SolidColorBrush)new BrushConverter().ConvertFromString(s);
            kolor = b;
            update();
        }
        public int getStan()
        {
            return stan;
        }
        public void setStan(int s)
        {
            stan = s;
        }

        public Rectangle getRectangle()
        {
            return rectangle;
        }

        public void setGranica(Boolean b)
        {
            granica = b;
        }
       public Boolean getGranica()
        {
            return granica;
        }
        public void dodajRo(double r)
        {
            ro += r;
        }
        public double getRo()
        {
            return ro;
        }
        public void setBu(Boolean bu)
        {
            this.bu = bu;
        }
        public Boolean getBu()
        {
            return bu;
        }

    }
}

