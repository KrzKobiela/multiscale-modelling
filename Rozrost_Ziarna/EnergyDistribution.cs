﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Rozrost_Ziarna
{
    class EnergyDistribution
    {
        Komorka[,] tab;
        int col, row,enIn,enBo;
        public EnergyDistribution(Komorka[,] komorki, int col, int row,int enInside,int enBoundaries)
        {
            tab = komorki;
            this.col = col;
            this.row = row;
            enIn = enInside;
            enBo = enBoundaries;
        }

        public Komorka[,] HeteroDistribution()
        {
            foreach (Komorka k in tab)
            {
                if (k.getGranica() == true)
                {
                    k.setEnergy(enBo);
                    SolidColorBrush kolor = new SolidColorBrush(Color.FromRgb((byte)255, (byte)255, (byte)0));
                    k.setKolor((Brush)kolor);
                }
                else
                {
                    k.setEnergy(enIn);            
                    SolidColorBrush kolor = new SolidColorBrush(Color.FromRgb((byte)0, (byte)0, (byte)255));
                    k.setKolor((Brush)kolor);
                }
                k.setStan(0);
                k.update();
            }
            return tab;
        }

        public Komorka[,] HomoDistribution()
        {
            foreach (Komorka k in tab)
            {
                k.setEnergy(enIn);
                k.setStan(0);
                SolidColorBrush kolor = new SolidColorBrush(Color.FromRgb((byte)0, (byte)0, (byte)255));
                k.setKolor((Brush)kolor);
                k.update();
            }
            return tab;
        }
    }
}
